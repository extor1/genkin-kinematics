#include <unistd.h>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <params.h>
#include <config.h>
#include "sixaxeskinematics.h"

//######################################################################################################################
int32_t main(int32_t argc, char *argv[])
{
    JointVector v;
    SixAxesKinematics kinematics;

  //  std::cout << CONFIG_PATH << std::endl;

 //   srand(time(NULL));
    rand();
    arma::mat jacobi;
    for(unsigned int jointIndex = 0; jointIndex < 6 ; jointIndex++){
        double minAngle = kinematics.p_[jointIndex].minAngle;
        double range = kinematics.p_[jointIndex].maxAngle - kinematics.p_[jointIndex].minAngle;
        v.at(jointIndex) = 0.0;//minAngle+range*rand()/RAND_MAX;
    }

    std::cout << v.at(0) << ", " << v.at(1) << ", " << v.at(2) << ", " << v.at(3) << ", " << v.at(4) << ", " << v.at(5) << std::endl;
    jacobi = kinematics.getJacobian(v,false);
    std::cout << jacobi << std::endl << std::endl;

    arma::mat jacobi2;
    jacobi2 = arma::zeros(6,6);
    arma::mat T0 = kinematics.getEePose(v);
    //std::cout << T0(0,3) << " " << T0(1,3) << " " << T0(2,3) << std::endl;
    JointVector v2 = kinematics.getJointAngles(T0,v);
 //   std::cout << v2.at(0) << ", " << v2.at(1) << ", " << v2.at(2) << ", " << v2.at(3) << ", " << v2.at(4) << ", " << v2.at(5) << std::endl;
    double x0;
    double y0;
    double z0;
    double roll0;
    double pitch0;
    double yaw0;
    x0 = T0(0,3);
    y0 = T0(1,3);
    z0 = T0(2,3);
    kinematics.getRPY(T0,roll0,pitch0,yaw0);

    // get the jacobian from finite differences
 /*   double delta = 0.0005;
    for(unsigned int jointIndex = 0; jointIndex < 6 ; jointIndex++){
        v.at(jointIndex) = v.at(jointIndex) + delta;
        arma::mat T_delta = kinematics.getEePose(v);
        double x_delta;
        double y_delta;
        double z_delta;
        double roll_delta;
        double pitch_delta;
        double yaw_delta;
        x_delta = T_delta(0,3);
        y_delta = T_delta(1,3);
        z_delta = T_delta(2,3);
        kinematics.getRPY(T_delta,roll_delta,pitch_delta,yaw_delta);
        jacobi2.at(0,jointIndex) = (x_delta-x0)/delta;
        jacobi2.at(1,jointIndex) = (y_delta-y0)/delta;
        jacobi2.at(2,jointIndex) = (z_delta-z0)/delta;
        jacobi2.at(3,jointIndex) = (roll_delta-roll0)/delta;
        jacobi2.at(4,jointIndex) = (pitch_delta-pitch0)/delta;
        jacobi2.at(5,jointIndex) = (yaw_delta-yaw0)/delta;

        v.at(jointIndex) = v.at(jointIndex) - delta;
    }
    std::cout << jacobi2 << std::endl << std::endl;*/

    return 0;
}

#ifndef COMMON_HELPERS_H
#define COMMON_HELPERS_H

#include <sstream>
#include <unistd.h>
#include <iostream>
#include <vector>

#include "version.h"


inline void printVersion()
{
    std::stringstream ss;
    ss << "===== | ";
#ifdef GIT_DATE
    ss << GIT_DATE << " | ";
#endif
#ifdef GIT_BRANCH
    ss << GIT_BRANCH << " | ";
#endif
#ifdef GIT_COMMIT
    ss << GIT_COMMIT << " | ";
#endif
    ss << "=====";

#ifdef LOG_TO_FILE
    std::ofstream(LOGFILE, std::ios_base::out | std::ios_base::app ) << ss.str() << std::endl;
#else
    std::cout << ss.str() << std::endl;
#endif
}

inline std::vector<std::string> splitString(const std::string &s, char delim)
{
    std::stringstream ss(s);
    std::string item;
    std::vector<std::string> tokens;
    while (getline(ss, item, delim))
    {
        tokens.push_back(item);
    }
    return tokens;
}

static inline void msleep(uint32_t milliseconds)
{
    sleep(milliseconds / 1000);
    usleep((milliseconds % 1000) * 1000);
}

static inline void fsleep(double seconds)
{
    if(seconds >= 1.0)
        sleep((int)seconds);

    usleep((uint32_t)(seconds * 1000000) % 1000000);
}

#endif

#ifndef TRAFO_H
#define TRAFO_H

#include <array>
#include <string>
#include <math.h>
#include <armadillo>

#ifndef M_PI
#define M_PI (3.14159265358979323846)
#endif

#define bzero(b,len) (memset((b), '\0', (len)), (void) 0)

class Vector4
{
public:
    Vector4();

    inline double* data(){return data_.data();}
    inline const double* const_data()const{return data_.data();}
protected:
    std::array<double, 16> data_;
};

class TfMatrix
{
public:
    TfMatrix();
    void initFromFloatArray(float* data);
    void initFromDoubleArray(double* data);
    void getRPY(double& roll, double& pitch, double& yaw);
    void getXYZ(double& x, double& y, double& z);
    void setRPY(double roll, double pitch, double yaw);
    void setXYZ(double x, double y, double z);
    void setX(double x){data_[3]=x;}
    void setY(double y){data_[7]=y;}
    void setZ(double z){data_[11]=z;}
    void setIndex(unsigned int i, double value){data_[i] = value;}
    double& x(){return data_[3];}
    double& y(){return data_[7];}
    double& z(){return data_[11];}
    double& at(unsigned int index){return  data_[index];}
    void setIdentity();
    TfMatrix& operator*=(const TfMatrix& m2);
    friend TfMatrix operator*(const TfMatrix& rhs, TfMatrix lhs);
    std::string toString();
    std::string toString(std::string label);
    void clearRotation();
    void setYaw(double yaw);
    double getYaw();
    bool invert();
    TfMatrix inverse();
    void translate(double x, double y, double z);
    void copyToFloatArray(float* dest) const;
    inline double* data(){return data_.data();}
    inline const double* const_data()const{return data_.data();}
    double roll_;
    double pitch_;
    double yaw_;

protected:

    std::array<double, 16> data_;

};

/*class Quaternion
{
public:
    Quaternion();
    void initFromDoubleArray(double* data);
    setQuaternion(TfMatrix x);
    TfMatrix getRotation();

    inline double* data(){return data_.data();}
    inline const double* const_data()const{return data_.data();}
protected:
    std::array<double, 16> data_;
};*/

Vector4 operator*(const TfMatrix& mat, const Vector4& vec);

#endif // TRAFO_H

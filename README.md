### General Kinematics: C++ Library for Robotics
Simon Kleinschmidt @forward-ttc  12.10.2018

Operating System: Windows

################################
1. External Librarys 
Required Open Source Library: armadillo (http://arma.sourceforge.net)
has to be compiled  at ../libs
Modifications at armadillo/include/armadillo_bits/config.hpp:

uncomment lines 19 and 26 such as:
//#define ARMA_USE_LAPACK
//#define ARMA_USE_BLAS

################################
2. Change Parameters:
Go to include/denavit-hartenberg.conf
change DH-parameters to your robot (all in radiant, e.g. meters)

If axes are defined in not-DH-convention and show in different directions, change the axis i to:
dhconventioni: false

If axes are translation axes, change the axis i to:
ji: false

mini and maxi represent the minimum and maximum values of axis i
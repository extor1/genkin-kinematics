#ifndef COMMON_MESSAGE_H
#define COMMON_MESSAGE_H
#include <string>
#include <cstring>

struct MessageHeader
{
    MessageHeader(): type(0), size(0){}
    MessageHeader(uint32_t t, uint32_t s): type(t), size(s){}
    uint32_t size;
    uint32_t type;
};

struct CString : public std::array<char, 128>
{
    CString(){}
    CString(std::string s){bzero(this->data(), this->size());memcpy(this->data(), s.data(), std::min(this->size()-1, s.size()));}
    CString(const char* c){bzero(this->data(), this->size());std::string s(c);memcpy(this->data(), s.data(), std::min(this->size()-1, s.size()));}
    std::string str() const{return std::string(this->data());}
};

inline std::ostream& operator<<(std::ostream& os, const CString& obj)
{
    os << obj.str();
    return os;
}
#endif

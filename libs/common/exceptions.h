#ifndef COMMON_EXCEPTIONS_H
#define COMMON_EXCEPTIONS_H

struct ExceptionWithMessage : public std::exception
{
    ExceptionWithMessage(){}
    ExceptionWithMessage(std::string msg): msg_(msg){}
    const char * what () const throw ()
    {
        return msg_.c_str();
    }

private:
    std::string msg_;
};

struct TimeoutException : public ExceptionWithMessage
{
    TimeoutException():ExceptionWithMessage("Timeout"){}
    TimeoutException(std::string msg):ExceptionWithMessage(msg){}
};

struct AbortedException : public ExceptionWithMessage
{
    AbortedException():ExceptionWithMessage("Aborted"){}
    AbortedException(std::string msg):ExceptionWithMessage(msg){}
};

struct NotImplementedException : public ExceptionWithMessage
{
    NotImplementedException():ExceptionWithMessage("Not yet implemented"){}
    NotImplementedException(std::string msg):ExceptionWithMessage(msg){}
};
#endif

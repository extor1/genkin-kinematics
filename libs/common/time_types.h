#ifndef COMMON_TIME_TYPES_H
#define COMMON_TIME_TYPES_H

#include <chrono>
#include <cstdint>

#define TIME_NOW std::chrono::high_resolution_clock::now()
//#define MILLIS_SINCE(x) std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - x).count();

typedef std::chrono::high_resolution_clock::time_point TimePoint;
typedef std::chrono::high_resolution_clock::duration Duration;

inline uint64_t hoursSince(const TimePoint &t)
{
    return std::chrono::duration_cast<std::chrono::hours>(std::chrono::high_resolution_clock::now() - t).count();
}

inline uint64_t minutesSince(const TimePoint &t)
{
    return std::chrono::duration_cast<std::chrono::minutes>(std::chrono::high_resolution_clock::now() - t).count();
}

inline uint64_t secondsSince(const TimePoint &t)
{
    return std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - t).count();
}

inline uint64_t millisecondsSince(const TimePoint &t)
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - t).count();
}

#endif

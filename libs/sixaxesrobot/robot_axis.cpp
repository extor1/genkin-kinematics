#include <iomanip>
#include <iostream>
#include <unistd.h>
#include <math.h>
#include <params.h>
#include <config.h>
#include "robot_axis.h"

// #####################################################################################################################
RobotAxis::RobotAxis()
{
    params_.gear_ratio = 0;
    params_.max_acceleration = 0;
    params_.max_velocity = 0;
    params_.reference_position = 0;
    params_.num_steps = 0;
    params_.unit_factor = 0;
    params_.min_position = 0;
    params_.max_position = 0;
    params_.direction = 1.0;
    params_.homing_velocity = 0.0;
}

// #####################################################################################################################
RobotAxis::RobotAxis(std::string name)
    : RobotAxis()
{
    loadParams(name);
}

// #####################################################################################################################
double RobotAxis::stepsToPos(double steps)
{
    return params_.direction * steps / params_.num_steps / params_.gear_ratio * params_.unit_factor;
}

// #####################################################################################################################
double RobotAxis::posToSteps(double position)
{
    return params_.direction * position / params_.unit_factor * params_.gear_ratio * params_.num_steps;
}

// #####################################################################################################################
double RobotAxis::maxVel()
{
    return params_.max_velocity;
}

// #####################################################################################################################
double RobotAxis::maxAcc()
{
    return params_.max_acceleration;
}

// #####################################################################################################################
double RobotAxis::maxPos()
{
    return params_.max_position;
}

// #####################################################################################################################
double RobotAxis::minPos()
{
    return params_.min_position;
}

// #####################################################################################################################
double RobotAxis::resolution()
{
    return 1.0 / params_.num_steps / params_.gear_ratio * params_.unit_factor;
}

// #####################################################################################################################
void RobotAxis::loadParams(std::string name)
{
    parameters::ParamSet p;
    p = parameters::load(AXPARAMS_FILE, name);

    params_.name = name;
    params_.gear_ratio         = p.getDouble("gear_ratio");
    params_.max_acceleration   = p.getDouble("max_acceleration");
    params_.max_velocity       = p.getDouble("max_velocity");
    params_.reference_position = p.getDouble("reference_position");
    params_.num_steps          = p.getDouble("num_steps");
    params_.unit_factor        = p.getDouble("unit_factor");
    params_.min_position       = p.getDouble("min_position");
    params_.max_position       = p.getDouble("max_position");
    params_.direction          = p.getDouble("direction");
    params_.homing_velocity    = p.getDouble("homing_velocity");
    params_.display_factor     = p.getDouble("display_factor");
    params_.display_unit       = p.getString("display_unit");
}

// #####################################################################################################################
std::string RobotAxis::getPositionString(double position)
{
    std::stringstream ss;
    const int num_decimals = 2;
    ss << std::setw(5 + num_decimals) << std::fixed << std::setprecision(num_decimals) << position * params_.display_factor << " " << params_.display_unit;
    return ss.str();
}

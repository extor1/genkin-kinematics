#ifndef COMMON_LOGGING_H
#define COMMON_LOGGING_H

#include <iomanip>
#include <iostream>
#include <sstream>
#include <sys/time.h>

#define PRECISION_STAMP



#ifdef LOG_TO_FILE
   #define MSG_RAW std::ofstream(LOGFILE, std::ios_base::out | std::ios_base::app )
   #define MSG_INFO stamp(std::ofstream(LOGFILE, std::ios_base::out | std::ios_base::app ))
   #define MSG_ERROR stamp(std::ofstream(LOGFILE, std::ios_base::out | std::ios_base::app )) << "(ERROR)"
#else
   #define MSG_RAW std::cout
   #define MSG_INFO stamp(std::cout)
   #define MSG_ERROR stamp(std::cout) << "(ERROR)"
#endif

#ifdef SHOW_DEBUG_LOG
 #ifdef LOG_TO_FILE
    #define MSG_DEBUG stamp(std::ofstream(LOGFILE, std::ios_base::out | std::ios_base::app )) << "(DEBUG)"
 #else
    #define MSG_DEBUG stamp(std::cout) <<  "(DEBUG)"
 #endif
#else
 #define MSG_DEBUG if(true){}else std::cerr
#endif

#ifdef PRECISION_STAMP
inline std::ostream& stamp(std::ostream& os)
{
    struct timeval time_now;
    gettimeofday(&time_now, NULL);

    return os << std::setfill('0') << "["
             << std::setw(10) << time_now.tv_sec << "."
             << std::setw(6) << time_now.tv_usec
             << "] ";
}
#else
inline std::ostream& stamp(std::ostream& os)
{
    time_t now = time(0);
    tm *ltm = localtime(&now);

    os << std::setfill('0') << "[";

#ifdef LOG_DATE
    os << std::setw(2) << ltm->tm_mday << "."
       << std::setw(2) << ltm->tm_mon << "."
       << std::setw(4) << 1970 + ltm->tm_year << "-";
#endif

    os << std::setw(2) << ltm->tm_hour << ":"
       << std::setw(2) << ltm->tm_min << ":"
       << std::setw(2) << ltm->tm_sec // << "."
       << "] ";
    return os;
}
#endif


inline std::string timestamp()
{
    std::stringstream ss;
    time_t now = time(0);
    tm *ltm = localtime(&now);

    ss << "[" << ltm->tm_hour << ":" << ltm->tm_min << ":" << ltm->tm_sec << "]";
    return "[" + ss.str() + "] ";
}

//inline std::string timestamp()
//{
//    auto t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
//    std::string s(std::ctime(&t));
//    s.pop_back();
//    return "[" + s + "] ";
//}
#endif

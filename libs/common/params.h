/**
 * @file params.h
 * @author Simon Aden
 * @copyright Copyright (c) 2018 forward ttc GmbH
 * @brief Functions for reading config files
 */

#ifndef COMMON_PARAMETERS_H
#define COMMON_PARAMETERS_H

#include <map>
#include <fstream>
#include <string>
#include <algorithm>
//#include <pwd.h>
#include "helpers.h"

namespace parameters
{

/**
 * @brief The ParameterNotFoundException struct
 */
struct ParameterNotFoundException : public std::exception
{
    ParameterNotFoundException(){}
    ParameterNotFoundException(std::string msg): msg_(msg){}
    const char * what () const throw ()
    {
        return msg_.c_str();
    }

private:
    std::string msg_;
};

/**
 * @brief Represents a set of parameters optained from a config file
 */
class ParamSet
{
public:    
    bool getBool(std::string key)
    {
        if(!map_.count(key)) throw(ParameterNotFoundException(key));
        return map_[key] == "true" || map_[key] == "True" || map_[key] == "TRUE";
    }

    double getDouble(std::string key)
    {
        if(!map_.count(key)) throw(ParameterNotFoundException(key));
        return atof(map_[key].c_str());
    }

    float getFloat(std::string key)
    {
        if(!map_.count(key)) throw(ParameterNotFoundException(key));
        return atof(map_[key].c_str());
    }

    int32_t getInt(std::string key)
    {
        if(!map_.count(key)) throw(ParameterNotFoundException(key));
        return atoi(map_[key].c_str());
    }

    unsigned int getHexInt(std::string key)
    {        
        if(!map_.count(key)) throw(ParameterNotFoundException(key));
        return strtoul(map_[key].c_str(), nullptr, 16);
    }

    std::string getString(std::string key)
    {
        if(!map_.count(key)) throw(ParameterNotFoundException(key));
        return map_[key].c_str();
    }

    void set(std::string key, std::string value)
    {
        map_[key] = value;
    }

private:

    std::map<std::string, std::string> map_;
};

/**
 * @brief Load a config file
 *
 * Loads a yaml-like config file with simple key-value pairs and optional sections.
 * Example:
 * @code
 * --Section1-----
 * param1: 30 # comment 1
 * param2: 4.0
 *
 * # comment 2
 *
 * --Section2-----
 * param1: true
 * param2: /opt/ccr/bin
 * param3: 0x2f
 * @endcode
 *
 * @param filename Filename
 * @param section Optional section of parameter file
 * @return Set of parameters read from file
 */
inline ParamSet load(std::string filename, std::string section="")
{
    ParamSet params;
    std::ifstream file;
    file.open(filename);
    std::string current_section;

    if(file.is_open())
    {
        std::string line;
        while(std::getline(file, line))
        {
            if(line.empty() || line[0] == '#')
                continue;            

            if(line[0] == '-') // section start
            {
                line.erase(std::remove(line.begin(), line.end(), '-'), line.end());
                current_section = line;
            }
            else if(section.empty() || current_section == section)
            {
                // remove comments
                std::vector<std::string> comment_split = splitString(line, '#');
                line = comment_split[0];

                // remove spaces
                line.erase(std::remove(line.begin(), line.end(), ' '), line.end());

                // read key/value pair (separated by ':')
                std::vector<std::string> vs = splitString(line, ':');
                if(vs.size() == 2)
                    params.set(vs[0], vs[1]);

            }
        }
    }

    return params;
}

/**
 * @brief Get robot IP (read from file ~/.ccrconfig)
 * @return IP address as string
 */
/*inline std::string getRobotIp()
{
    // load config file
    struct passwd *pw = getpwuid(getuid());
    std::string filename(pw->pw_dir);
    filename.append("/.ccrconfig");
    std::string ip;
    std::ifstream ifile(filename);
    if(ifile.is_open())
    {
        std::getline(ifile, ip);
    }
    else
    {
        ip = "127.0.0.1";
        ifile.close();
        std::ofstream ofile(filename);
        ofile << ip << std::endl;
        ofile.close();
    }

    return ip;
}*/

}



#endif

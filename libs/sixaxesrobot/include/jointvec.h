#ifndef JOINTVEC_H
#define JOINTVEC_H

#include <array>
#include <strings.h>
#include <math.h>
#include <cmath>

#define N_ROBOT_AXES 6
#define bzero(b,len) (memset((b), '\0', (len)), (void) 0)

class JointVector : public std::array<double, N_ROBOT_AXES>
{
  public:
    JointVector():is_valid_(true){bzero(this->data(), N_ROBOT_AXES*sizeof(double));}
    void setValid(bool is_valid){is_valid_ = is_valid;}
    bool isValid()
    {
        if(!is_valid_)
            return false;
        else
        {
            for(int i=0; i<N_ROBOT_AXES; i++)
                if(std::isnan((*this)[i]))
                    return false;

            return true;
        }
    }

    JointVector& operator+=(const JointVector& rhs)
    {
        this->is_valid_ = this->is_valid_ && rhs.is_valid_;

        for(int32_t i=0; i<N_ROBOT_AXES;i++)
          (*this)[i] += rhs[i];
        return *this;
    }
    const JointVector operator+(const JointVector& rhs) const
    {
        JointVector j;
        j.is_valid_ = this->is_valid_ && rhs.is_valid_;

        for(int32_t i=0; i<N_ROBOT_AXES;i++)
            j[i] = (*this)[i] + rhs[i];
        return j;
    }
    JointVector& operator-=(const JointVector& rhs)
    {
        this->is_valid_ = this->is_valid_ && rhs.is_valid_;

        for(int32_t i=0; i<N_ROBOT_AXES;i++)
          (*this)[i] -= rhs[i];
        return *this;
    }
    const JointVector operator-(const JointVector& rhs) const
    {
        JointVector j;
        j.is_valid_ = this->is_valid_ && rhs.is_valid_;

        for(int32_t i=0; i<N_ROBOT_AXES;i++)
            j[i] = (*this)[i] - rhs[i];
        return j;
    }

    double dist() const
    {
        double val = 0;
        for(int32_t i=0; i<N_ROBOT_AXES;i++)
            val += (*this)[i] * (*this)[i];

        return sqrt(val);

    }

private:
    bool is_valid_;
};

typedef JointVector CartesianVector; /// same type, different names to differenciate jointspace and taskspace

#endif // JOINTVEC_H

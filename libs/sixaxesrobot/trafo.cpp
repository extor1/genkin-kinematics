#include "trafo.h"
#include <cstring>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <sstream>

Vector4::Vector4()
{
    bzero(data_.data(), sizeof(4 * sizeof(double)));
}

TfMatrix::TfMatrix()
{
    setIdentity();
}

void TfMatrix::setIdentity()
{
    bzero(data_.data(), 16 * sizeof(double));
    data_[0] = 1.0;
    data_[5] = 1.0;
    data_[10] = 1.0;
    data_[15] = 1.0;
}

void TfMatrix::initFromFloatArray(float* data)
{
    std::array<float, 16> f;
    memcpy(f.data(), data, 16 * sizeof(float));

    std::array<double, 16> d;
    for(int32_t i=0; i<16;i++)
        d[i] = (double)f[i];

    initFromDoubleArray(d.data());
}

void TfMatrix::initFromDoubleArray(double* data)
{
    memcpy(data_.data(), data, 16*sizeof(double));
}

TfMatrix& TfMatrix::operator*=(const TfMatrix& m2)
{
    double a00 = this->data_[0];
    double a01 = this->data_[1];
    double a02 = this->data_[2];
    double a03 = this->data_[3];

    double a10 = this->data_[4];
    double a11 = this->data_[5];
    double a12 = this->data_[6];
    double a13 = this->data_[7];

    double a20 = this->data_[8];
    double a21 = this->data_[9];
    double a22 = this->data_[10];
    double a23 = this->data_[11];

    double a30 = this->data_[12];
    double a31 = this->data_[13];
    double a32 = this->data_[14];
    double a33 = this->data_[15];

    double b00 = m2.data_[0];
    double b01 = m2.data_[1];
    double b02 = m2.data_[2];
    double b03 = m2.data_[3];

    double b10 = m2.data_[4];
    double b11 = m2.data_[5];
    double b12 = m2.data_[6];
    double b13 = m2.data_[7];

    double b20 = m2.data_[8];
    double b21 = m2.data_[9];
    double b22 = m2.data_[10];
    double b23 = m2.data_[11];

    double b30 = m2.data_[12];
    double b31 = m2.data_[13];
    double b32 = m2.data_[14];
    double b33 = m2.data_[15];

    this->data_[0] = a00*b00+a10*b01+a20*b02+a30*b03;
    this->data_[1] = a01*b00+a11*b01+a21*b02+a31*b03;
    this->data_[2] = a02*b00+a12*b01+a22*b02+a32*b03;
    this->data_[3] = a03*b00+a13*b01+a23*b02+a33*b03;

    this->data_[4] = a00*b10+a10*b11+a20*b12+a30*b13;
    this->data_[5] = a01*b10+a11*b11+a21*b12+a31*b13;
    this->data_[6] = a02*b10+a12*b11+a22*b12+a32*b13;
    this->data_[7] = a03*b10+a13*b11+a23*b12+a33*b13;

    this->data_[8] = a00*b20+a10*b21+a20*b22+a30*b23;
    this->data_[9] = a01*b20+a11*b21+a21*b22+a31*b23;
    this->data_[10] = a02*b20+a12*b21+a22*b22+a32*b23;
    this->data_[11] = a03*b20+a13*b21+a23*b22+a33*b23;

    this->data_[12] = a00*b30+a10*b31+a20*b32+a30*b33;
    this->data_[13] = a01*b30+a11*b31+a21*b32+a31*b33;
    this->data_[14] = a02*b30+a12*b31+a22*b32+a32*b33;
    this->data_[15] = a03*b30+a13*b31+a23*b32+a33*b33;

    return *this;
}

TfMatrix operator*(const TfMatrix& rhs, TfMatrix lhs)
{
    lhs *= rhs;
    return lhs;
}

void TfMatrix::getRPY(double& roll, double& pitch, double& yaw)
{
    double r00 = data_[0];
    double r01 = data_[1];
    double r02 = data_[2];
    double r10 = data_[4];
    double r11 = data_[5];
    double r12 = data_[6];
    double r20 = data_[8];
    double r21 = data_[9];
    double r22 = data_[10];

    if(r20 < 1.0)
    {
        if(r20 > -1.0)
        {
            pitch = asin(-r20);           
            yaw =  atan2(r10, r00);
            roll = atan2(r21, r22);
        }
        else
        {
            // singular
            pitch = M_PI/2;
            yaw = -atan2(-r12, r11);
            roll = 0.0;            
        }
    }
    else
    {
        // singular
        pitch = -M_PI / 2;
        yaw   = atan2(-r12, r11);
        roll  = 0.0;        
    }
    pitch_ = pitch;
    yaw_ = yaw;
    roll_ = roll;
}

void TfMatrix::getXYZ(double& x, double& y, double& z)
{
    x = data_[3];
    y = data_[7];
    z = data_[11];
}

void TfMatrix::setRPY(double roll, double pitch, double yaw)
{
    TfMatrix rx, ry, rz;
    rx.data_[5] = cos(roll);
    rx.data_[6] = -sin(roll);
    rx.data_[9] = sin(roll);
    rx.data_[10] = cos(roll);

    ry.data_[0] = cos(pitch);
    ry.data_[2] = sin(pitch);
    ry.data_[8] = -sin(pitch);
    ry.data_[10] = cos(pitch);

    rz.data_[0] = cos(yaw);
    rz.data_[1] = -sin(yaw);
    rz.data_[4] = sin(yaw);
    rz.data_[5] = cos(yaw);

    double x,y,z;
    this->getXYZ(x,y,z);
    this->setIdentity();

    (*this) *= (rz * ry * rx);  

    this->setXYZ(x,y,z);
}

void TfMatrix::clearRotation()
{
    double x,y,z;
    this->getXYZ(x,y,z);
    this->setIdentity();
    this->setXYZ(x,y,z);
}

void TfMatrix::setXYZ(double x, double y, double z)
{
    data_[3] = x;
    data_[7] = y;
    data_[11] = z;
}

std::string TfMatrix::toString()
{
    return toString("");
}

std::string TfMatrix::toString(std::string label)
{
    std::stringstream ss;
    if(!label.empty())
    {
        ss << label << ":" << std::endl;
    }

    for(int32_t i=0; i<16;i++)
    {
        ss << std::fixed << std::setw(7) << std::setprecision(4) << data_[i];
        if(i%4 != 3)
            ss << "   ";
        else
            ss <<std::endl;
    }

    double r,p,y;
    this->getRPY(r,p,y);
    ss << "RPY: " << r*180/M_PI << "; " << p*180/M_PI << "; " << y*180/M_PI << std::endl;

    return ss.str();
}

void TfMatrix::setYaw(double yaw)
{
    double r, p, y;
    this->getRPY(r, p, y);
    this->setRPY(r, p, yaw);
}

double TfMatrix::getYaw()
{
    double r,p,y;
    this->getRPY(r, p, y);
    return y;
}

bool TfMatrix::invert()
{
    double inv[16], det;
    int32_t i;

    inv[0] = data_[5]  * data_[10] * data_[15] -
             data_[5]  * data_[11] * data_[14] -
             data_[9]  * data_[6]  * data_[15] +
             data_[9]  * data_[7]  * data_[14] +
             data_[13] * data_[6]  * data_[11] -
             data_[13] * data_[7]  * data_[10];

    inv[4] = -data_[4]  * data_[10] * data_[15] +
              data_[4]  * data_[11] * data_[14] +
              data_[8]  * data_[6]  * data_[15] -
              data_[8]  * data_[7]  * data_[14] -
              data_[12] * data_[6]  * data_[11] +
              data_[12] * data_[7]  * data_[10];

    inv[8] = data_[4]  * data_[9] * data_[15] -
             data_[4]  * data_[11] * data_[13] -
             data_[8]  * data_[5] * data_[15] +
             data_[8]  * data_[7] * data_[13] +
             data_[12] * data_[5] * data_[11] -
             data_[12] * data_[7] * data_[9];

    inv[12] = -data_[4]  * data_[9] * data_[14] +
               data_[4]  * data_[10] * data_[13] +
               data_[8]  * data_[5] * data_[14] -
               data_[8]  * data_[6] * data_[13] -
               data_[12] * data_[5] * data_[10] +
               data_[12] * data_[6] * data_[9];

    inv[1] = -data_[1]  * data_[10] * data_[15] +
              data_[1]  * data_[11] * data_[14] +
              data_[9]  * data_[2] * data_[15] -
              data_[9]  * data_[3] * data_[14] -
              data_[13] * data_[2] * data_[11] +
              data_[13] * data_[3] * data_[10];

    inv[5] = data_[0]  * data_[10] * data_[15] -
             data_[0]  * data_[11] * data_[14] -
             data_[8]  * data_[2] * data_[15] +
             data_[8]  * data_[3] * data_[14] +
             data_[12] * data_[2] * data_[11] -
             data_[12] * data_[3] * data_[10];

    inv[9] = -data_[0]  * data_[9] * data_[15] +
              data_[0]  * data_[11] * data_[13] +
              data_[8]  * data_[1] * data_[15] -
              data_[8]  * data_[3] * data_[13] -
              data_[12] * data_[1] * data_[11] +
              data_[12] * data_[3] * data_[9];

    inv[13] = data_[0]  * data_[9] * data_[14] -
              data_[0]  * data_[10] * data_[13] -
              data_[8]  * data_[1] * data_[14] +
              data_[8]  * data_[2] * data_[13] +
              data_[12] * data_[1] * data_[10] -
              data_[12] * data_[2] * data_[9];

    inv[2] = data_[1]  * data_[6] * data_[15] -
             data_[1]  * data_[7] * data_[14] -
             data_[5]  * data_[2] * data_[15] +
             data_[5]  * data_[3] * data_[14] +
             data_[13] * data_[2] * data_[7] -
             data_[13] * data_[3] * data_[6];

    inv[6] = -data_[0]  * data_[6] * data_[15] +
              data_[0]  * data_[7] * data_[14] +
              data_[4]  * data_[2] * data_[15] -
              data_[4]  * data_[3] * data_[14] -
              data_[12] * data_[2] * data_[7] +
              data_[12] * data_[3] * data_[6];

    inv[10] = data_[0]  * data_[5] * data_[15] -
              data_[0]  * data_[7] * data_[13] -
              data_[4]  * data_[1] * data_[15] +
              data_[4]  * data_[3] * data_[13] +
              data_[12] * data_[1] * data_[7] -
              data_[12] * data_[3] * data_[5];

    inv[14] = -data_[0]  * data_[5] * data_[14] +
               data_[0]  * data_[6] * data_[13] +
               data_[4]  * data_[1] * data_[14] -
               data_[4]  * data_[2] * data_[13] -
               data_[12] * data_[1] * data_[6] +
               data_[12] * data_[2] * data_[5];

    inv[3] = -data_[1] * data_[6] * data_[11] +
              data_[1] * data_[7] * data_[10] +
              data_[5] * data_[2] * data_[11] -
              data_[5] * data_[3] * data_[10] -
              data_[9] * data_[2] * data_[7] +
              data_[9] * data_[3] * data_[6];

    inv[7] = data_[0] * data_[6] * data_[11] -
             data_[0] * data_[7] * data_[10] -
             data_[4] * data_[2] * data_[11] +
             data_[4] * data_[3] * data_[10] +
             data_[8] * data_[2] * data_[7] -
             data_[8] * data_[3] * data_[6];

    inv[11] = -data_[0] * data_[5] * data_[11] +
               data_[0] * data_[7] * data_[9] +
               data_[4] * data_[1] * data_[11] -
               data_[4] * data_[3] * data_[9] -
               data_[8] * data_[1] * data_[7] +
               data_[8] * data_[3] * data_[5];

    inv[15] = data_[0] * data_[5] * data_[10] -
              data_[0] * data_[6] * data_[9] -
              data_[4] * data_[1] * data_[10] +
              data_[4] * data_[2] * data_[9] +
              data_[8] * data_[1] * data_[6] -
              data_[8] * data_[2] * data_[5];

    det = data_[0] * inv[0] + data_[1] * inv[4] + data_[2] * inv[8] + data_[3] * inv[12];

    if (det == 0)
        return false;

    det = 1.0 / det;

    for (i = 0; i < 16; i++)
        data_[i] = inv[i] * det;

    return true;
}

TfMatrix TfMatrix::inverse()
{
    TfMatrix m = (*this);
    if(!m.invert())
        bzero(m.data_.data(), 16 * sizeof(double));

    return m;
}

void TfMatrix::translate(double x, double y, double z)
{
    data_[3] += x;
    data_[7] += y;
    data_[11] += z;
}

void TfMatrix::copyToFloatArray(float* dest) const
{
//    std::vector<float> f(16);
    float f[16];
    for(int32_t i=0; i<16;i++)
        f[i] = (float)data_[i];

    memcpy(dest, f, 16 * sizeof(float));
}

Vector4 operator*(const TfMatrix& mat, const Vector4& vec)
{
    Vector4 result;
    const double* mdata = mat.const_data();
    const double* vdata = vec.const_data();
    double* rdata = result.data();
    rdata[0] = mdata[0]  * vdata[0] + mdata[1]  * vdata[1] + mdata[2]  * vdata[2] + mdata[3]  * vdata[2];
    rdata[1] = mdata[4]  * vdata[0] + mdata[5]  * vdata[1] + mdata[6]  * vdata[2] + mdata[7]  * vdata[2];
    rdata[2] = mdata[8]  * vdata[0] + mdata[9]  * vdata[1] + mdata[10] * vdata[2] + mdata[11] * vdata[2];
    rdata[3] = mdata[12] * vdata[0] + mdata[13] * vdata[1] + mdata[14] * vdata[2] + mdata[15] * vdata[2];
    return result;
}
/*
Quaternion::Quaternion()
{
    bzero(data_.data(), sizeof(4 * sizeof(double)));
}

TfMatrix Quaternion::getRotation()
{
    TfMatrix mat;
    mat.setIdentity();
    mat[0] = data_[0]*data[0] + data_[1]*data[1] - data_[2]*data[2] - data_[3]*data[3];
    mat[1] = 2*(data_[1]*data[2] + data_[0]*data[3]);
    mat[2] = 2*(data_[1]*data[3] - data_[0]*data[2]);
    mat[4] = 2*(data_[1]*data[2] - data_[0]*data[3]);
    mat[5] = data_[0]*data[0] - data_[1]*data[1] + data_[2]*data[2] - data_[3]*data[3];
    mat[6] = 2*(data_[2]*data[3] + data_[0]*data[1]);
    mat[6] = 2*(data_[1]*data[3] + data_[0]*data[2]);
    mat[8] = 2*(data_[2]*data[3] - data_[0]*data[1]);
    mat[10] = data_[0]*data[0] - data_[1]*data[1] - data_[2]*data[2] + data_[3]*data[3];
}

Quaternion& Quaternion::operator*=(const Quaternion& q2)
{
    double a0 = this->data_[0];
    double a1 = this->data_[1];
    double a2 = this->data_[2];
    double a3 = this->data_[3];

    double b0 = q2.data_[0];
    double b1 = q2.data_[1];
    double b2 = q2.data_[2];
    double b3 = q2.data_[3];

    this->data_[0] = a0*b0-a1*b1-a2*b2-a3*b3;
    this->data_[1] = a0*b1+a1*b0+a2*b3-a3*b2;
    this->data_[2] = a0*b2-a1*b3+a2*b0+a3*b1;
    this->data_[3] = a0*b3+a1*b2-a2*b1+a3*b0;

    return *this;
}

Quaternion operator*(const Quaternion& q1, Quaternion q2)
{
    q2 *= q1;
    return lhs;
}*/

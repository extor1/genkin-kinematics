#ifndef SIXAXESKINEMATICS_H
#define SIXAXESKINEMATICS_H

#include <array>
#include <strings.h>
#include "trafo.h"
#include "jointvec.h"
#include "robot_axis.h"

#define N_ROBOT_AXES 6

struct AxesParams
{
    double theta, d, a, alpha, minAngle, maxAngle;
    bool rotAxis, conventional;
};

class SixAxesKinematics
{
public:
    SixAxesKinematics(); /// Automatically loads kinematic parameters from config file

    /**
     * @brief Direct kinematics
     * @param q Vector of joint positions
     * @return End effector pose (tcp in base coordinates) as transformation matrix
     */
    arma::mat getEePose(JointVector q);

    /**
     * @brief Direct kinematics of Joint 2
     * @param q Vector of joint positions
     * @return Pose of Joint2 (tcp in base coordinates) as transformation matrix
     */
    arma::mat getJoint2(JointVector q);

    /**
     * @brief Direct kinematics of Joint 2
     * @param q Vector of joint positions
     * @return Pose of Joint3 (tcp in base coordinates) as transformation matrix
     */
    arma::mat getJoint3(JointVector q);

    /**
     * @brief Direct kinematics of Joint 2
     * @param q Vector of joint positions
     * @return Pose of Joint4 (tcp in base coordinates) as transformation matrix
     */
    arma::mat getJoint4(JointVector q);

    /**
     * @brief Inverse kinematics
     * @param x End effector position (tcp in base coordinates) as transformation matrix
     * @param reference Reference position to solve ambiguity
     * @return Vector of joint angles
     */
    JointVector getJointAngles(arma::mat x, JointVector reference);
    /**
     * @brief Inverse kinematics
     * @param x End effector position (tcp in base coordinates) as transformation matrix
     * @param reference Reference position to solve ambiguity
     * @param transformation matrix from fland to tool coordinateframe
     * @return Vector of joint angles
     */
    JointVector getJointAngles(arma::mat x, JointVector reference, arma::mat tool_offset);

    /**
     * @brief Get cartesian velocity of the end effector (tcp in base coordinates)
     * @param joint_velocity Vector of joint velocities
     * @param joint_position Vector of joint positions
     * @return Cartesian Velocity (x,y,z,roll,pitch,yaw)
     */
    CartesianVector getEeVelocity(JointVector joint_velocity, JointVector joint_position);

    /**
     * @brief Get joint velocity
     * @param end_effector_velocity Cartesian end effector velocity (tcp in base coordintes)
     * @param joint_position Vector of joint positions
     * @return Vector of joint velocities
     */
    JointVector getJointVelocity(CartesianVector end_effector_velocity, JointVector joint_position);

    /**
     * @brief Get force/torque at end effector from joint forces/torques
     * @param joint_force Vector of joint forces/torques
     * @param joint_position Vector of joint positions
     * @return Cartesian forces/torques at end effector (in base coordinates)
     */
    CartesianVector getEeForce(JointVector joint_force, JointVector joint_position);

    /**
     * @brief Get forces/torques at the joints from end effector force/torque
     * @param end_effector_force Force/Torque at end effector (in base coordinates)
     * @param joint_position Vector of joint positions
     * @return Vector of joint forces/torques
     */
    JointVector getJointForce(CartesianVector end_effector_force, JointVector joint_position);

    /**
     * @brief Get information about sinularity
     * @param joint_position Vector of joint positions
     * @return boolean, if robot is not in singular position
     */
    bool noSingularity(JointVector joint_position);

    /**
     * @brief Set parameters for given axis from existing RobotAxis object.
     *        This is only needed for reachability check (min and max joint positions).
     * @param index Index of axis for which parameters will be set (starting with 0)
     * @param axis Existing RobotAxis object
     */
    void setAxParams(int index, const RobotAxis* axis);

    /**
     * @brief Set parameters for given axis by loading from config file
     *        This is only needed for reachability check (min and max joint positions).
     * @param index Index of axis for which parameters will be set (starting with 0)
     * @param name Name of the axis (as defined in config file, e.g. "Joint 1")
     */
    void loadAxParams(int index, std::string name);

    /**
     * @brief Checks if the given end effector pose (tcp in base coordinates) is reachable.
     *        AxParams must be set. Kinematics and joint limits are evaluated.
     * @param pose_base End effector pose (tcp in base coordinates) as transformation matrix
     * @return True if pose is reachable
     */
    bool isReachable(arma::mat pose_base);

    /**
     * @brief Get kinematic parameters
     * @return Kinematic parameters
     */
   // KinematicParams& params(){return p_;}
    AxesParams& params(){return p_[N_ROBOT_AXES];}

    arma::mat getJacobian(JointVector joint_position, bool spaceJacobian);

    static void getRPY(arma::mat tf, double& roll, double& pitch, double& yaw, bool xyz_convention = true);

    static void getRPY_OLD(arma::mat tf, double& roll, double& pitch, double& yaw, bool xyz_convention = true);

    static void getTransform(double roll, double pitch, double yaw, arma::mat &tf, bool xyz_convention = true);

    static void getTransform_OLD(double roll, double pitch, double yaw, arma::mat &tf, bool xyz_convention = true);


    void printJacobian();

    AxesParams p_[N_ROBOT_AXES];

protected:

 //   arma::mat getJacobian(JointVector joint_position);
    //KinematicParams p_; /// Kinematic parameters
    std::array<RobotAxis, N_ROBOT_AXES> axes_; /// Robot axes (needed for reachability check)
};

#endif // SIXAXESKINEMATICS_H

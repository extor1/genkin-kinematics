#ifndef ROBOT_AXIS_H
#define ROBOT_AXIS_H
#include <vector>
#include <string>

struct AxisParams
{
    std::string name;
    std::string display_unit; /// unit symbol (mm,deg,...) only needed for printing
    double num_steps;       /// incl. microstep multiplier
    double gear_ratio;      /// > 1
    double unit_factor;     /// 1 (m) or 2*pi
    double reference_position; /// reference position for homing
    double max_velocity;
    double max_acceleration;
    double max_position;
    double min_position;
    double direction;
    double homing_velocity;
    double display_factor; /// multiplication factor for printing (e.g. 1000 to print m in mm)

};

class RobotAxis
{
public:
    RobotAxis();
    RobotAxis(std::string name);
    AxisParams& params(){return params_;}
    double maxVel();
    double maxAcc();
    double minPos();
    double maxPos();
    std::string name(){return params_.name;}
    double resolution();
    double stepsToPos(double steps);
    double posToSteps(double position);    
    void loadParams(std::string name);
    std::string getPositionString(double position); /// in printable format with unit

protected:
    AxisParams params_;    
};

#endif // ROBOT_AXIS_H

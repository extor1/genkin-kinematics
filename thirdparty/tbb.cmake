# --------------------------------------------------------------------
# Download and build Armadillo

set(armadillo_RELEASE "9.900.x")
set(armadillo_BRANCH ${armadillo_RELEASE})
set(ARMADILLO_CXX_VERSION "")
set(DARWIN_DISABLE_HDF5 "")
set(ARMADILLO_TOOLCHAIN_FILE "")
set(PARALLEL_BUILD "--parallel 2")

set(ARMADILLO_BUILD_COMMAND "cmake"
        "--build" "${CMAKE_BINARY_DIR}/armadillo-${armadillo_RELEASE}"
        "--config" $<$<CONFIG:Debug>:Debug>$<$<CONFIG:Release>:Release>$<$<CONFIG:RelWithDebInfo>:RelWithDebInfo>$<$<CONFIG:MinSizeRel>:MinSizeRel>$<$<CONFIG:NoOptWithASM>:Debug>$<$<CONFIG:Coverage>:Debug>$<$<CONFIG:O2WithASM>:RelWithDebInfo>$<$<CONFIG:O3WithASM>:RelWithDebInfo>$<$<CONFIG:ASAN>:Debug>
)    

message(STATUS " Armadillo ${armadillo_RELEASE} will be downloaded and built automatically")    

ExternalProject_Add(
        armadillo
        PREFIX ${CMAKE_BINARY_DIR}/armadillo-${armadillo_RELEASE}
        GIT_REPOSITORY https://gitlab.com/conradsnicta/armadillo-code.git
        GIT_TAG ${armadillo_BRANCH}
        SOURCE_DIR ${CMAKE_BINARY_DIR}/thirdparty/armadillo/armadillo-${armadillo_RELEASE}
        BINARY_DIR ${CMAKE_BINARY_DIR}/armadillo-${armadillo_RELEASE}
        CMAKE_ARGS -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
            -DBUILD_SHARED_LIBS=OFF
            ${ARMADILLO_CXX_VERSION}
            ${DARWIN_DISABLE_HDF5}
            ${ARMADILLO_TOOLCHAIN_FILE}
            -DCMAKE_BUILD_TYPE=$<$<CONFIG:Debug>:Debug>$<$<CONFIG:Release>:Release>$<$<CONFIG:RelWithDebInfo>:RelWithDebInfo>$<$<CONFIG:MinSizeRel>:MinSizeRel>$<$<CONFIG:NoOptWithASM>:Debug>$<$<CONFIG:Coverage>:Debug>$<$<CONFIG:O2WithASM>:RelWithDebInfo>$<$<CONFIG:O3WithASM>:RelWithDebInfo>$<$<CONFIG:ASAN>:Debug>
        BUILD_COMMAND "${ARMADILLO_BUILD_COMMAND} ${PARALLEL_BUILD}"
        BUILD_BYPRODUCTS ${CMAKE_BINARY_DIR}/armadillo-${armadillo_RELEASE}/${CMAKE_FIND_LIBRARY_PREFIXES}armadillo${CMAKE_STATIC_LIBRARY_SUFFIX}
        UPDATE_COMMAND ""
        INSTALL_COMMAND ""
)

set (ARMADILLO_HEADERS ${CMAKE_BINARY_DIR}/thirdparty/armadillo/armadillo-${armadillo_RELEASE}/include)
set (ARMADILLO_LIBRARIES ${CMAKE_BINARY_DIR}/armadillo-${armadillo_RELEASE}/libarmadillo.a)



cmake_minimum_required(VERSION 3.16)
project(sixachsesrobot)


set(GENKIN_SOURCES
    trafo.cpp
    sixaxeskinematics.cpp
    robot_axis.cpp
)

find_package(LAPACK)
find_package(BLAS)

## headers files
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../common)

add_library(sixaxesrobot ${GENKIN_SOURCES})
add_dependencies(sixaxesrobot armadillo)
target_link_libraries(sixaxesrobot ${ARMADILLO_LIBRARIES} ${BLAS_LIBRARIES} ${LAPACK_LIBRARIES})

#target_include_directories(sixaxesrobot PUBLIC "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>"
#"$<INSTALL_INTERFACE:$<INSTALL_PREFIX>/${CMAKE_INSTALL_INCLUDEDIR}>")

add_executable(test_jacobi test_jacobi.cpp)
target_link_libraries(test_jacobi sixaxesrobot ${ARMADILLO_LIBRARIES} ${BLAS_LIBRARIES} ${LAPACK_LIBRARIES})

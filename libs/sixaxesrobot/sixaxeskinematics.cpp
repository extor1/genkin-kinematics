#include <math.h>
#include <random>
#include <logging.h>
#include <params.h>
#include <config.h>
#include "sixaxeskinematics.h"

// #####################################################################################################################
SixAxesKinematics::SixAxesKinematics()
{
    parameters::ParamSet params = parameters::load(KINEMATICS_FILE);

    for(int i=0; i<N_ROBOT_AXES; i++)
    {
        std::string numberString = std::to_string(i+1);

        std::string paramName = "theta" + numberString;
        p_[i].theta =  params.getDouble(paramName);

        paramName = "dhconvention" + numberString;
        p_[i].conventional = params.getBool(paramName);

        paramName = "d" + numberString;
        p_[i].d = params.getDouble(paramName);

        paramName = "a" + numberString;
        p_[i].a = params.getDouble(paramName);

        paramName = "alpha" + numberString;
        p_[i].alpha = params.getDouble(paramName);

        paramName = "min" + numberString;
        p_[i].minAngle = params.getDouble(paramName);

        paramName = "max" + numberString;
        p_[i].maxAngle = params.getDouble(paramName);

        paramName = "j" + numberString;
        p_[i].rotAxis = params.getBool(paramName);
    }

}

// #####################################################################################################################
arma::mat SixAxesKinematics::getEePose(JointVector q)
{

    arma::mat tf = arma::eye(4,4);
    for(unsigned int axesNo = 0; axesNo < N_ROBOT_AXES; axesNo++){
        if(!p_[axesNo].conventional){
            q.at(axesNo) = -q.at(axesNo);
        }
        arma::mat tfi = arma::eye(4,4);
        double a = p_[axesNo].a;
        double theta = p_[axesNo].theta;
        double d = p_[axesNo].d;
        double alpha = p_[axesNo].alpha;
        if(p_[axesNo].rotAxis){
            theta = theta + q.at(axesNo);
        }
        else{
            d = d + q.at(axesNo);
        }
        double matData[16] = {cos(theta),sin(theta),0,0,-sin(theta)*cos(alpha),cos(theta)*cos(alpha),sin(alpha),0,
                         sin(theta)*sin(alpha),-cos(theta)*sin(alpha),cos(alpha),0,a*cos(theta),a*sin(theta),d,1};
        //std::cout << matData << std::endl;
        for(unsigned i = 0; i< 16; i++){
            tfi(i%4,i/4) = matData[i];
        }
        tf = tf*tfi;
    }

    return tf;
}

// #####################################################################################################################
arma::mat SixAxesKinematics::getJoint2(JointVector q)
{
    int N_JOINT = 2;
    arma::mat tf = arma::eye(4,4);
    for(unsigned int axesNo = 0; axesNo < N_JOINT; axesNo++){
        if(!p_[axesNo].conventional){
            q.at(axesNo) = -q.at(axesNo);
        }
        arma::mat tfi = arma::eye(4,4);
        double a = p_[axesNo].a;
        double theta = p_[axesNo].theta;
        double d = p_[axesNo].d;
        double alpha = p_[axesNo].alpha;
        if(p_[axesNo].rotAxis){
            theta = theta + q.at(axesNo);
        }
        else{
            d = d + q.at(axesNo);
        }
        double matData[16] = {cos(theta),sin(theta),0,0,-sin(theta)*cos(alpha),cos(theta)*cos(alpha),sin(alpha),0,
                         sin(theta)*sin(alpha),-cos(theta)*sin(alpha),cos(alpha),0,a*cos(theta),a*sin(theta),d,1};
        //std::cout << matData << std::endl;
        for(unsigned i = 0; i< 16; i++){
            tfi(i%4,i/4) = matData[i];
        }
        tf = tf*tfi;
    }

    return tf;
}

// #####################################################################################################################
arma::mat SixAxesKinematics::getJoint3(JointVector q)
{
    int N_JOINT = 3;
    arma::mat tf = arma::eye(4,4);
    for(unsigned int axesNo = 0; axesNo < N_JOINT; axesNo++){
        if(!p_[axesNo].conventional){
            q.at(axesNo) = -q.at(axesNo);
        }
        arma::mat tfi = arma::eye(4,4);
        double a = p_[axesNo].a;
        double theta = p_[axesNo].theta;
        double d = p_[axesNo].d;
        double alpha = p_[axesNo].alpha;
        if(p_[axesNo].rotAxis){
            theta = theta + q.at(axesNo);
        }
        else{
            d = d + q.at(axesNo);
        }
        double matData[16] = {cos(theta),sin(theta),0,0,-sin(theta)*cos(alpha),cos(theta)*cos(alpha),sin(alpha),0,
                         sin(theta)*sin(alpha),-cos(theta)*sin(alpha),cos(alpha),0,a*cos(theta),a*sin(theta),d,1};
        //std::cout << matData << std::endl;
        for(unsigned i = 0; i< 16; i++){
            tfi(i%4,i/4) = matData[i];
        }
        tf = tf*tfi;
    }

    return tf;
}

// #####################################################################################################################
arma::mat SixAxesKinematics::getJoint4(JointVector q)
{
    int N_JOINT = 4;
    arma::mat tf = arma::eye(4,4);
    for(unsigned int axesNo = 0; axesNo < N_JOINT; axesNo++){
        if(!p_[axesNo].conventional){
            q.at(axesNo) = -q.at(axesNo);
        }
        arma::mat tfi = arma::eye(4,4);
        double a = p_[axesNo].a;
        double theta = p_[axesNo].theta;
        double d = p_[axesNo].d;
        double alpha = p_[axesNo].alpha;
        if(p_[axesNo].rotAxis){
            theta = theta + q.at(axesNo);
        }
        else{
            d = d + q.at(axesNo);
        }
        double matData[16] = {cos(theta),sin(theta),0,0,-sin(theta)*cos(alpha),cos(theta)*cos(alpha),sin(alpha),0,
                         sin(theta)*sin(alpha),-cos(theta)*sin(alpha),cos(alpha),0,a*cos(theta),a*sin(theta),d,1};
        //std::cout << matData << std::endl;
        for(unsigned i = 0; i< 16; i++){
            tfi(i%4,i/4) = matData[i];
        }
        tf = tf*tfi;
    }

    return tf;
}

// #####################################################################################################################
JointVector SixAxesKinematics::getJointAngles(arma::mat tf, JointVector reference)
{
    for(unsigned int axesNo = 0; axesNo < N_ROBOT_AXES; axesNo++){
        if(!p_[axesNo].conventional){
            reference.at(axesNo) = -reference.at(axesNo);
        }
    }
    JointVector q;
    double xEE, yEE, zEE;
    xEE = tf(0,3);
    yEE = tf(1,3);
    zEE = tf(2,3);
    double rollEE, pitchEE, yawEE;
    getRPY(tf, rollEE, pitchEE, yawEE);
    // 1. Get wrist Point
    double x_wrist = xEE - tf(0,2)*p_[5].d;
    double y_wrist = yEE - tf(1,2)*p_[5].d;
    double z_wrist = zEE - tf(2,2)*p_[5].d;

    // 2. solve inverse kinematics of main axes
    double l3 = p_[1].a; double l5 = p_[3].d; double l4 = p_[2].a;
    double k1, k2, k3, k5, k6;

    if(p_[0].a + l3*sin(reference[1]) +l4*sin(reference[1]+reference[2]) + l5*cos(reference[1]+reference[2]) > 0){
        k3 = sqrt(x_wrist*x_wrist + y_wrist*y_wrist) - p_[0].a;
    }
    else{
         k3 = -sqrt(x_wrist*x_wrist + y_wrist*y_wrist) - p_[0].a;
    }
    k5 = (z_wrist-p_[0].d)*(z_wrist-p_[0].d) + k3*k3;
    k6 = (k5 - l5*l5 - l4*l4 - l3*l3)/(2*l3);
    if(k6*k6 >l5*l5+l4*l4){
        q[2] = atan2(k6 , -sqrt(-l5*l5-l4*l4+k6*k6)) + atan(l4/l5);
    }
    else{
        q[2] = -atan2(k6 , sqrt(l5*l5+l4*l4-k6*k6)) + atan(l4/l5);
    }


    k1 = -l5*sin(q[2])+l4*cos(q[2]) + l3;
    k2 = l4*sin(q[2]) + l5*cos(q[2]);
    q[1] = atan2(k3,z_wrist-p_[0].d) - atan2(k2,k1);
    if(p_[0].a + l3*sin(reference[1]) +l4*sin(reference[1]+reference[2]) + l5*cos(reference[1]+reference[2]) > 0){
        q[0] = atan2(y_wrist,x_wrist);
    }
    else{
        q[0] = atan2(-y_wrist,-x_wrist);
    }

    // 3. orientation/pose of last KS of main axes
    arma::mat tf_wrist = arma::eye(4,4);
    for(unsigned int axesNo = 0; axesNo < 3; axesNo++){
        arma::mat tf_i = arma::eye(4,4);
        double a = p_[axesNo].a;
        double theta = p_[axesNo].theta;
        double d = p_[axesNo].d;
        double alpha = p_[axesNo].alpha;
        if(p_[axesNo].rotAxis){
            theta = theta + q.at(axesNo);
        }
        else{
            d = d + q.at(axesNo);
        }
        double matData[16] = {cos(theta),sin(theta),0,0,-sin(theta)*cos(alpha),cos(theta)*cos(alpha),sin(alpha),0,
                         sin(theta)*sin(alpha),-cos(theta)*sin(alpha),cos(alpha),0,a*cos(theta),a*sin(theta),d,1};
        for(unsigned i = 0; i< 16; i++){

            tf_i(i%4,i/4) = matData[i];
        }
        tf_wrist = tf_wrist*tf_i;
    }
    // 4. calculate remaining rotation/transformation to end effector
    arma::mat tf_remaining = tf_wrist.i()*tf;
    // 5. Solve inverse kinematics of last axes
    if(sin(reference[4]) > 0.00001){
        q[3] = atan2(-tf_remaining(1,2),-tf_remaining(0,2));
        q[4] = atan2(sqrt(tf_remaining(2,0)*tf_remaining(2,0)+tf_remaining(2,1)*tf_remaining(2,1)),tf_remaining(2,2));
        q[5] = atan2(-tf_remaining(2,1),tf_remaining(2,0));
    }
    else{
        q[3] = atan2(tf_remaining(1,2),tf_remaining(0,2));
        q[4] = atan2(-sqrt(tf_remaining(2,0)*tf_remaining(2,0)+tf_remaining(2,1)*tf_remaining(2,1)),tf_remaining(2,2));
        q[5] = atan2(tf_remaining(2,1),-tf_remaining(2,0));
    }

    // if singular
    if(tf_remaining(2,2) > 0.99999){
        q[3]=atan2(tf_remaining(1,0),tf_remaining(0,0));
        q[5]=0;
    }
    else if(tf_remaining(2,2) < -0.99999){
        q[3]=atan2(-tf_remaining(1,0),-tf_remaining(0,0));
        q[5]=0;
    }

    // turn axes 4 and 6 to get near the old configuration
    if(reference[3] - q[3] > 1.2*M_PI && q[3] < 0){
        q[3] = q[3] + 2*M_PI;
    }
    else if(reference[3] - q[3] < -1.2*M_PI && q[3] > 0){
        q[3] = q[3] - 2*M_PI;
    }

    if(reference[5] - q[5] > 1.2*M_PI && q[5] < 0){
        q[5] = q[5] + 2*M_PI;
    }
    else if(reference[5] - q[5] < -1.2*M_PI && q[5] > 0){
        q[5] = q[5] - 2*M_PI;
    }

    for(unsigned int axesNo = 0; axesNo < N_ROBOT_AXES; axesNo++){
        if(!p_[axesNo].conventional){
            q.at(axesNo) = -q.at(axesNo);
        }
    }

    return q;
}

JointVector SixAxesKinematics::getJointAngles(arma::mat tf, JointVector reference, arma::mat tool_offset){
    arma::mat tf_ee = arma::eye(4,4);
    tf_ee = tf*inv(tool_offset);
    return getJointAngles(tf_ee, reference);
}

// #####################################################################################################################
void SixAxesKinematics::setAxParams(int index, const RobotAxis* axis)
{
    axes_[index] = *axis;
}

// #####################################################################################################################
void SixAxesKinematics::loadAxParams(int index, std::string name)
{
    axes_[index] = RobotAxis(name);
}

// #####################################################################################################################
bool SixAxesKinematics::isReachable(arma::mat pose_base)
{
    JointVector reference;
    for(int i=0; i<N_ROBOT_AXES; i++)
        reference[i] = axes_[i].params().reference_position;

    JointVector q = getJointAngles(pose_base, reference);

    if(!q.isValid())
    {
        MSG_DEBUG << "Inverse kinematics not solvable." << std::endl;
       // MSG_DEBUG << pose_base.toString("Unsolvable Pose:");
        MSG_DEBUG << "Unsolvable Pose";
        return false;
    }

    for(unsigned int i=0; i<N_ROBOT_AXES; i++)
    {
        if(q[i] < axes_[i].minPos() || q[i] > axes_[i].maxPos())
        {
            if(q[i] < axes_[i].minPos())
                MSG_DEBUG << "Joint limit violation in joint " << i << " (" << q[i] << " < " << axes_[i].minPos() << ")" << std::endl;
            else if(q[i] > axes_[i].maxPos())
                MSG_DEBUG << "Joint limit violation in joint " << i << " (" << q[i] << " > " << axes_[i].maxPos() << ")" << std::endl;
            return false;
        }
    }

    return true;
}

// #####################################################################################################################
arma::mat SixAxesKinematics::getJacobian(JointVector joint_position, bool spaceJacobian)
{
    arma::mat jacobian = arma::zeros(6,6);

    for(unsigned int jacobiColumn = 0; jacobiColumn< N_ROBOT_AXES; jacobiColumn ++){
        arma::mat dTrafo_dq = arma::eye(4,4);
        arma::mat Trafo = arma::eye(4,4);
        for(unsigned int axesNo = 0; axesNo < N_ROBOT_AXES; axesNo++){
            if(!p_[axesNo].conventional){
                joint_position.at(axesNo) = -joint_position.at(axesNo);
            }

            arma::mat Trafoi = arma::zeros(4,4);

            double a = p_[axesNo].a;
            double theta = p_[axesNo].theta;
            double d = p_[axesNo].d;
            double alpha = p_[axesNo].alpha;
            if(p_[axesNo].rotAxis){
                theta = theta + joint_position.at(axesNo);
            }
            else{
                d = d + joint_position.at(axesNo);
            }
            Trafoi.at(0,0) = cos(theta);
            Trafoi.at(1,0) = sin(theta);
            Trafoi.at(0,1) = -sin(theta)*cos(alpha);
            Trafoi.at(1,1) = cos(theta)*cos(alpha);
            Trafoi.at(2,1) = sin(alpha);
            Trafoi.at(0,2) = sin(theta)*sin(alpha);
            Trafoi.at(1,2) = -cos(theta)*sin(alpha);
            Trafoi.at(2,2) = cos(alpha);
            Trafoi.at(0,3) = a*cos(theta);
            Trafoi.at(1,3) = a*sin(theta);
            Trafoi.at(2,3) = d;
            Trafoi.at(3,3) = 1;

            Trafo = Trafo*Trafoi;
            if(jacobiColumn != axesNo){
                // normal trafo and trafo for derivation are the same
            }
            else{
                Trafoi = arma::zeros(4,4);
                if(p_[axesNo].rotAxis){
                    Trafoi.at(0,0) = -sin(theta);
                    Trafoi.at(1,0) = cos(theta);
                    Trafoi.at(0,1) = -cos(theta)*cos(alpha);
                    Trafoi.at(1,1) = -sin(theta)*cos(alpha);
                    Trafoi.at(0,2) = cos(theta)*sin(alpha);
                    Trafoi.at(1,2) = sin(theta)*sin(alpha);
                    Trafoi.at(0,3) = -a*sin(theta);
                    Trafoi.at(1,3) = a*cos(theta);
                }
                else{
                    Trafoi.at(2,3) = 1;
                }
            }
            dTrafo_dq = dTrafo_dq*Trafoi;
        }
        arma::mat skewMat;
        if(spaceJacobian){
            skewMat = dTrafo_dq*Trafo.i();
        }
        else{
            skewMat = Trafo.i()*dTrafo_dq;
            dTrafo_dq = Trafo*dTrafo_dq;
        }

        jacobian.at(0,jacobiColumn) = dTrafo_dq.at(0,3);
        jacobian.at(1,jacobiColumn) = dTrafo_dq.at(1,3);
        jacobian.at(2,jacobiColumn) = dTrafo_dq.at(2,3);
        jacobian.at(3,jacobiColumn) = 0.5*(skewMat.at(2,1)-skewMat.at(1,2));
        jacobian.at(4,jacobiColumn) = 0.5*(skewMat.at(0,2)-skewMat.at(2,0));
        jacobian.at(5,jacobiColumn) = 0.5*(skewMat.at(1,0)-skewMat.at(0,1));
    }

    return jacobian;
}

// #####################################################################################################################
CartesianVector SixAxesKinematics::getEeVelocity(JointVector joint_velocity, JointVector joint_position)
{
    arma::vec q_dot = arma::zeros(6);
    arma::vec x_dot = arma::zeros(6);

    CartesianVector end_effector_velocity;

    for(unsigned int i = 0; i < N_ROBOT_AXES; i++){
        q_dot.at(i) = joint_velocity[i];
    }
    x_dot = getJacobian(joint_position,true)*q_dot;
    for(unsigned int i = 0; i < N_ROBOT_AXES; i++){
       end_effector_velocity[i] = x_dot.at(i);
    }
    return end_effector_velocity;
}

// #####################################################################################################################
JointVector SixAxesKinematics::getJointVelocity(CartesianVector end_effector_velocity, JointVector joint_position)
{

    arma::vec q_dot = arma::zeros(6);
    arma::vec x_dot = arma::zeros(6);

    JointVector joint_velocity;

    for(unsigned int i = 0; i < N_ROBOT_AXES; i++){
        x_dot.at(i) = end_effector_velocity[i];
    }
    q_dot = getJacobian(joint_position,true).i()*x_dot;
    for(unsigned int i = 0; i < N_ROBOT_AXES; i++){
       joint_velocity[i] = q_dot.at(i);
    }
    return joint_velocity;
}

// #####################################################################################################################
CartesianVector SixAxesKinematics::getEeForce(JointVector joint_force, JointVector joint_position)
{
    arma::vec F = arma::zeros(6);
    arma::vec tau = arma::zeros(6);

    CartesianVector end_effector_force;

    for(unsigned int i = 0; i < N_ROBOT_AXES; i++){
        tau.at(i) = joint_force[i];
    }
    F = arma::solve(getJacobian(joint_position,true).t(),tau);
    for(unsigned int i = 0; i < N_ROBOT_AXES; i++){
       end_effector_force[i] = F.at(i);
    }
    return end_effector_force;
}

// #####################################################################################################################
JointVector SixAxesKinematics::getJointForce(CartesianVector end_effector_force, JointVector joint_position)
{
    arma::vec F = arma::zeros(6);
    arma::vec tau = arma::zeros(6);

    JointVector joint_force;

    for(unsigned int i = 0; i < N_ROBOT_AXES; i++){
        F.at(i) = end_effector_force[i];
    }
    tau = getJacobian(joint_position,true).t()*F;
    for(unsigned int i = 0; i < N_ROBOT_AXES; i++){
       joint_force[i] = tau.at(i);
    }
    return joint_force;
}

// #####################################################################################################################
bool SixAxesKinematics::noSingularity(JointVector joint_position)
{
    if(arma::det(getJacobian(joint_position,true)) > 0.0001){
        return true;
    }
    else{
        return false;
    }
}

// #####################################################################################################################
void SixAxesKinematics::getRPY(arma::mat tf, double& roll, double& pitch, double& yaw, bool xyz_convention)
{
    double r00 = tf(0,0);
    double r01 = tf(0,1);
    double r02 = tf(0,2);
    double r10 = tf(1,0);
    double r11 = tf(1,1);
    double r12 = tf(1,2);
    double r20 = tf(2,0);
    double r21 = tf(2,1);
    double r22 = tf(2,2);
    if(xyz_convention){

        if (fabs(fabs(r02) - 1) < 0.01){
                      // singularity
                      yaw = 0;  //roll is zero
                      if (r02 > 0){
                        roll = atan2(r21, r11);   // R+Y
                      }else{
                           roll = -atan2( r10, r20);  // R-Y
                      }
                      pitch = asin(r02);
         }else{
                      yaw = -atan2(r01, r00);
                      roll = -atan2(r12, r22);

                     pitch = atan(r02*cos(roll)/r00);
        }
    }else{
        if (fabs(fabs(r20) - 1) < 0.01){
                      // singularity

                      yaw = 0;     // roll is zero
                      if (r20 < 0){
                          roll = -atan2(r01, r02);  // R-Y
                      }else{
                          roll = atan2(-r01, -r02);  // R+Y
                      }
                      pitch = -asin(r20);
        } else{
                      yaw = atan2(r21, r22);  // R
                      roll = atan2(r10, r00);  // Y

                      pitch = -atan(r20*cos(roll)/r22);
        }
    }

}

void SixAxesKinematics::getRPY_OLD(arma::mat tf, double& roll, double& pitch, double& yaw, bool xyz_convention)
{
    double r00 = tf(0,0);
    double r01 = tf(1,0);
    double r02 = tf(2,0);
    double r10 = tf(0,1);
    double r11 = tf(1,1);
    double r12 = tf(2,1);
    double r20 = tf(0,2);
    double r21 = tf(1,2);
    double r22 = tf(2,2);



    if(xyz_convention){
        if(r20 < 1.0)
        {
            if(r20 > -1.0)
            {
                pitch = asin(-r20);
                yaw =  atan2(r10, r00);
                roll = atan2(r21, r22);
            }
            else
            {
                // singular
                pitch = M_PI/2;
                yaw = -atan2(-r12, r11);
                roll = 0.0;
            }
        }
        else
        {
            // singular
            pitch = -M_PI / 2;
            yaw   = atan2(-r12, r11);
            roll  = 0.0;
        }
    }else{
        if(r02 < 1.0)
        {
            if(r02 > -1.0)
            {
                pitch = asin(-r02);
                yaw =  atan2(r01, r00);
                roll = atan2(r12, r22);
            }
            else
            {
                // singular
                pitch = M_PI/2;
                yaw = -atan2(-r21, r11);
                roll = 0.0;
            }
        }
        else
        {
            // singular
            pitch = -M_PI / 2;
            yaw   = atan2(-r21, r11);
            roll  = 0.0;
        }
    }

}


// #####################################################################################################################
void SixAxesKinematics::getTransform(double roll, double pitch, double yaw, arma::mat &tf, bool xyz_convention){

    arma::mat rotx = arma::eye(4,4);
    arma::mat roty = arma::eye(4,4);
    arma::mat rotz = arma::eye(4,4);
    rotx(1,1) = cos(roll);
    rotx(1,2) = -sin(roll);
    rotx(2,1) = sin(roll);
    rotx(2,2) = cos(roll);

    roty(0,0) = cos(pitch);
    roty(0,2) = sin(pitch);
    roty(2,0) = -sin(pitch);
    roty(2,2) = cos(pitch);

    rotz(0,0) = cos(yaw);
    rotz(0,1) = -sin(yaw);
    rotz(1,0) = sin(yaw);
    rotz(1,1) = cos(yaw);

    if(xyz_convention){
       tf = rotx*roty*rotz;
    }else{
        tf = rotz*roty*rotx;
    }

}

// #####################################################################################################################
void SixAxesKinematics::getTransform_OLD(double roll, double pitch, double yaw, arma::mat &tf, bool xyz_convention){
    if(xyz_convention){
        tf(0,0) = cos(pitch)*cos(yaw);
        tf(0,1) = sin(roll)*sin(pitch)*cos(yaw)-cos(roll)*sin(yaw);
        tf(0,2) = cos(roll)*sin(pitch)*cos(yaw)+ sin(roll)*sin(yaw);
        tf(1,0)= cos(pitch)*sin(yaw);
        tf(1,1) = sin(roll)*sin(pitch)*sin(yaw) + cos(roll)*cos(yaw);
        tf(1,2) = cos(roll)*sin(pitch)*sin(yaw) - sin(roll)*cos(yaw);
        tf(2,0) = -sin(pitch);
        tf(2,1)= sin(roll)*cos(pitch);
        tf(2,2)= cos(roll)*cos(pitch);
    }else{
        tf(0,0) = cos(pitch)*cos(yaw);
        tf(1,0) = sin(roll)*sin(pitch)*cos(yaw)-cos(roll)*sin(yaw);
        tf(2,0) = cos(roll)*sin(pitch)*cos(yaw)+ sin(roll)*sin(yaw);
        tf(0,1)= cos(pitch)*sin(yaw);
        tf(1,1) = sin(roll)*sin(pitch)*sin(yaw) + cos(roll)*cos(yaw);
        tf(2,1) = cos(roll)*sin(pitch)*sin(yaw) - sin(roll)*cos(yaw);
        tf(0,2) = -sin(pitch);
        tf(1,2)= sin(roll)*cos(pitch);
        tf(2,2)= cos(roll)*cos(pitch);
    }

}

// #####################################################################################################################
void SixAxesKinematics::printJacobian(){
    JointVector q;
    for(unsigned int i = 0; i<6; i++){
        q.at(i) = 0;
    }
    arma::mat J = getJacobian(q,true);
    std::cout << J << std::endl;
}
